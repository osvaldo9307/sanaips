<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Facturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->id();
            $table->date('fechaIngreso');
            $table->integer('nFactura');
            $table->integer('idTercero');
            $table->integer('centroCostos');
            $table->integer('plazo')->nullable();
            $table->integer('idVendedor');
            $table->string('observaciones')->nullable();
            $table->json('productoServicio');                    
            $table->integer('descuento')->nullable();
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
