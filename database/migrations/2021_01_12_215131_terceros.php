<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Terceros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    

        Schema::create('terceros', function (Blueprint $table) {
            $table->id();
            $table->string('typeIdentification');
            $table->integer('identification');
            $table->integer('dv')->nullable();
            $table->string('firstName')->nullable();;
            $table->string('secondName')->nullable();;
            $table->string('firtsLastName')->nullable();;
            $table->string('secondLastName')->nullable();;
            $table->string('razonSocial')->nullable();;
            $table->string('address');
            $table->string('city');
            $table->string('typeTercero');
            $table->integer('phone');
            $table->string('rLegal')->nullable();
            $table->string('acompañante')->nullable();
            $table->boolean('rFuente')->nullable();
            $table->boolean('rIVA')->nullable();
            $table->boolean('rIndustraComercio')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terceros');
    }
}
