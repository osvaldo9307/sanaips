<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\RegistrarIngreso;
use App\Http\Livewire\RegistrarTercero;
use App\Http\Livewire\RegisterUser;
use App\Http\Livewire\RegistrarFactura;
use App\Http\Livewire\RegistrarEmpleados;
use App\Http\Livewire\RegistrarProductoServicio;

use App\Http\Livewire\FormBookAppointment;
use App\Http\Livewire\Citas\CalendarioCitas;
use App\Http\Livewire\Citas\ReservaCitas;
use App\Http\Livewire\Citas\ListaCitas;


Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
// Route::middleware(['auth:sanctum', 'verified'])->get('/register-user', function () {
//     return view('livewire.register-user');
// })->name('register-user');
Route::middleware(['auth:sanctum', 'verified'])->get('/register-role', function () {
    return view('livewire.register-role');
})->name('register-role');
// Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-tercero', function () {
//     return view('livewire.registrar-tercero');
// })->name('registrar-tercero');

Route::middleware(['auth:sanctum', 'verified'])->get('/register-user', RegisterUser::class)->name('register-user');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-ingreso', RegistrarIngreso::class)->name('registrar-ingreso');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-tercero', RegistrarTercero::class)->name('registrar-tercero');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-factura', RegistrarFactura::class)->name('registrar-factura');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-empleado', RegistrarEmpleados::class)->name('registrar-empleado');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-producto-servicio', RegistrarProductoServicio::class)->name('registrar-producto-servicio');

// nuevas rutas
Route::middleware(['auth:sanctum', 'verified'])->get('/reserva-citas', ReservaCitas::class)->name('Reserva_Citas');
Route::middleware(['auth:sanctum', 'verified'])->post('/lista-citas', [ReservaCitas::class,'create'])->name('Lista_CitasPos');
Route::middleware(['auth:sanctum', 'verified'])->get('/lista-citas', ListaCitas::class)->name('Lista_Citas');
Route::middleware(['auth:sanctum', 'verified'])->get('/calendario-citas', CalendarioCitas::class)->name('CalendarioCitas');

//Envio de formulario de citas
//Route::post('/HistorialCitasCliente', [ControllerCitas::class, 'create'])->name('HistorialCitasCliente');

