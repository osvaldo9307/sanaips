<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Terceros extends Model
{

    use HasFactory;
    protected $fillable = [
        'typeIdentification', 'identification', 'dv', 
        'secondName', 'firstName', 'firtsLastName', 'razonSocial', 
        'secondLastName', 'address', 'city', 'typeTercero', 'phone', 
        'rLegal', 'acompañante', 'rFuente', 'rIVA', 'rIndustraComercio',        
    ];

}
