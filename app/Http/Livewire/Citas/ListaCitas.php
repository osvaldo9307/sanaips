<?php

namespace App\Http\Livewire\Citas;

use Livewire\Component;

class ListaCitas extends Component
{
    public function render()
    {
        return view('livewire.Citas.lista-citas');
    }
}
