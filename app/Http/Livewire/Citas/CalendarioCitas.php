<?php

namespace App\Http\Livewire\Citas;

use Livewire\Component;

class CalendarioCitas extends Component
{
    public function render()
    {
        return view('livewire.Citas.calendario-citas');
    }
}
