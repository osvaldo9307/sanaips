<?php

namespace App\Http\Livewire\Citas;

use Livewire\Component;
use App\Models\Citas;
use DB;
use Session;



class ReservaCitas extends Component
{
    public function render()
    {

        // llamar base de datos
        $medicos=DB::table('terceros')->get();


        return view('livewire.Citas.reserva-citas',compact('medicos'));
    }

    public function create(){

        // validar formularios
          request()->validate([
               'Paciente'=> 'required|nullable',
               'medico'=> 'required|nullable',
               'fecha'=> 'required',
    
            ]);
           // envio a BD
           $paciente=request('Paciente');
           $medico=request('medico');
           $fecha=request('fecha');
    
          // Modelo
           Citas::create([
              'idtercero' => $paciente,
              'idmedico' => $medico,
              'fechaini' => $fecha,
              'fechafin' => $fecha,
              'observaciones'=> "",
              ]);
           // return view('livewire.Citas.lista-citas')->with('msj','Listo');

           return back()->with('estado','Listo');
    
            }
    
}
