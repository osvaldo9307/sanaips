<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RegistrarIngreso extends Component
{
    public function render()
    {
        return view('livewire.registrar-ingreso');
    }
}
