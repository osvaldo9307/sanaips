<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FormProductoServicio extends Component
{
    public $view = 'store';
    public $viewButton = 'store';
    public $identifier, $name, $type, $barraReferencia, $iva, $ico, $costo, $costoCalculado,
        $stock, $registroInvima, $talla, $pesoUnidad, $marca, $precioPublico, $valorUno, 
        $valorDos, $valorTres, $valorcuatro, $calIco, $calIva;
    public function render()
    {
        return view('livewire.form-producto-servicio');
    }
    public function changeView($data){       
        $this->view = $data;

    }
    public function calcularCosto(){
        if ($this->iva) {
            $this->calIva = ($this->costo * $this->iva/100);            
        }
        if ($this->ico) {
            $this->calIco = ($this->costo * $this->ico/100);
        }
        $this->costoCalculado = $this->costo + ($this->calIva+ $this->calIco);
    }
    public function store()
    {
        $messages = [
            'identifier.required' => 'El campo identificador es obligatorio ',
            'identifier.integer' => 'El campo identificador debe ser un número ',
            'name.required' => 'El campo nombre es obligatorio',
            'barraReferencia.required' => 'El campo barra/referencia es obligatorio',
            'costo.required' => 'El campo costo es obligatorio',
            'costoCalculado.required' => 'El campo costo total es obligatorio',           
            'precioPublico.required' => 'El campo precio publico es obligatorio',           
            'type.required' => 'El campo tipo es obligatorio',           
        ];
        $this->validate([
            'identifier' => 'required|integer',
            'name' => 'required',            
            'barraReferencia' => 'required',            
            'costo' => 'required',            
            'costoCalculado' => 'required',            
            'precioPublico' => 'required',            
            'type' => 'required',            
        ], $messages);

        User::create([
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'identification' => $this->identification,
            'typeIdentification' => $this->typeIdentification,
            'role' => $this->role,
            'password' => Hash::make($this->password),
        ]);
        $this->reset(['name', 'email', 'role', 'password', 'typeIdentification', 'identification']);
        $this->alert('success', 'Usuario creado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        // session()->flash('message', 'Usuario creado correctamente');

    }
}
