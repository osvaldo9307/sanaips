<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RegistrarEmpleados extends Component
{
    public function render()
    {
        return view('livewire.registrar-empleados');
    }
}
