<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Livewire\WithPagination;

class Tableusers extends Component
{   
    
    public function render()
    {
        return view('livewire.tableusers', [
            'users' => User::paginate(5)
        ]);
    }
    public function destroy($id)
    {
        if ($id) {
            $record = User::where('id', $id);
            $record->delete();
        }
    }
}
