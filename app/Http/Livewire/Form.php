<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Livewire\WithPagination;

class Form extends Component
{
    public $name, $email, $password, $role, $userId, $typeIdentification, $identification, $buscar;
    public $view = "store";
    protected $listeners = [
        'confirmed',
        'cancelled',        
    ];
    use WithPagination;
    public function render()
    {
        return view('livewire.form',[
            'users' => User::where('name', 'LIKE', "%{$this->buscar}%")
                ->orWhere('email', 'LIKE', "%{$this->buscar}%")
                ->orWhere('role', 'LIKE', "%{$this->buscar}%")
                ->orWhere('identification', 'LIKE', "%{$this->buscar}%")
                ->orderBy('id','DESC')
                ->paginate(6)
        ]);
    }
    public function store()
    {
        $messages = [
            'name.required' => 'El campo nombres y apellidos es obligatorio ',
            'email.required' => 'El campo correo es obligatorio',
            'email.unique' => 'El correo ya se encuentrea registrado',
            'email.email' => 'El correo no es valido',
            'password.required' => 'El campo contraseña es obligatorio',
            'typeIdentification.required' => 'El campo tipo de indentificación es obligatorio',
            'identification.required' => 'El campo indentificación es obligatorio',
        ];
        $this->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'role' => 'required',
            'password' => 'required',
            'identification' => 'required',
            'typeIdentification' => 'required',
        ], $messages);

        User::create([
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'identification' => $this->identification,
            'typeIdentification' => $this->typeIdentification,
            'role' => $this->role,
            'password' => Hash::make($this->password),
        ]);
        $this->reset(['name', 'email', 'role', 'password', 'typeIdentification', 'identification']);
        $this->alert('success', 'Usuario creado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        // session()->flash('message', 'Usuario creado correctamente');

    }
    public function confirmed()
    {
        // Example code inside confirmed callback
        User::destroy($this->userId);
        $this->alert(
            'success',
            'Usuario eliminado correctamente'
        );
    }

    public function cancelled()
    {
        // Example code inside cancelled callback

        $this->alert('info', 'Cancelado');
    }
    public function destroy($id)
    {   
        $this->userId = $id;
        $this->confirm('¿Estas seguro?', [
            'toast' => false,
            'position' => 'center',
            'showConfirmButton' => true,
            'confirmButtonText' =>  'Eliminar', 
            'cancelButtonText' => 'Cancelar',
            'onConfirmed' => 'confirmed',
            'onCancelled' => 'cancelled'
        ]);
        
    
        
    }
    public function cancel()
    {
        
        $this->reset(['name', 'email', 'role', 'password', 'typeIdentification', 'identification', 'view']);

    }
    public function edit($id)
    {
            $user = User::find($id);
            $this->userId = $user->id;
            $this->name = $user->name;
            $this->email = $user->email;
            $this->role = $user->role;
            $this->typeIdentification = $user->typeIdentification;
            $this->identification = $user->identification;

            $this->view = "edit";
    }
    public function update()
    {
        $messages = [
            'name.required' => 'El campo nombres y apellidos es obligatorio ',
            'email.required' => 'El campo correo es obligatorio',
            'email.unique' => 'El correo ya se encuentrea registrado',
            'email.email' => 'El correo no es valido',
            'typeIdentification.required' => 'El campo tipo de indentificación es obligatorio',
            'identification.required' => 'El campo indentificación es obligatorio',
            
        ];
        $this->validate([
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            'identification' => 'required',
            'typeIdentification' => 'required',
        ], $messages);
        $user = User::find($this->userId);
        $user->update([            
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'identification' => $this->identification,
            'typeIdentification' => $this->typeIdentification,
            
        ]);

        $this->reset(['name', 'email', 'role', 'password', 'typeIdentification', 'identification', 'view']);
        $this->alert('success', 'Usuario actualizado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

}
