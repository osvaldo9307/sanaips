<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;

class RegisterUser extends Component
{   
    public function render()
    {
        return view('livewire.register-user',[
            'users' => User::paginate()
        ]);
    }
    
    

    
}
