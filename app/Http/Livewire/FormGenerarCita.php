<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use DB;


class FormGenerarCita extends Component
{
    public function render()
    {

        // llamar base de datos
        $medicos=DB::table('terceros')->get();


        return view('livewire.Citas.form-generar-cita',compact('medicos'));
    }

}
