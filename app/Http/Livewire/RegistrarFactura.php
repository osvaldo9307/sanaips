<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RegistrarFactura extends Component
{
    public function render()
    {
        return view('livewire.registrar-factura');
    }
}
