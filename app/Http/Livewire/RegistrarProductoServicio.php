<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RegistrarProductoServicio extends Component
{
    public function render()
    {
        return view('livewire.registrar-producto-servicio');
    }
}
