<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Role;
class RegisterRole extends Component
{
    public $rolename = "medco" ;
    public $description;
    public function render()
    {
        return view('livewire.register-role',[
            'users' => Role::paginate(2)
        ]);
    }
    public function store()
    {
        $messages = [
            'name.required' => 'El campo nombre es obligatorio ',
            'description.required' => 'El descripción  es obligatorio',
            
        ];
        $this->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
        ], $messages);
        
        // User::create([
        //     'name' => $this->name,
        //     'email' => $this->email,
        //     'role' => $this->role,
        //     'password' => Hash::make($this->password),
        // ]);
        // $this->reset(['name', 'email', 'role', 'password']);

    }
}
