<?php

namespace App\Http\Livewire;

use App\Models\Facturas;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Empleados;

class FormFacturas extends Component
{
    
    public $consecutivo = 00001;
    public $nFactura = '00001';
    public $centroCostosName = 'Rubro';
    public $plazoName = 'X meses';

    public $view = 'store';
    public $viewButton = 'store';
    public $buscar, $buscarVendedor, $fechaIngreso, $prefijo, $plazo, $centroCostos,
        $idVendedor, $cEmpleado, $productoServicio, $descuento, $nameTercero, $adress, $city, $phone, $rLegal, $nameVendedor, $nameTerceroVendedor;

    public function doSomething(){
        $this->alert('success', 'calculando Iva!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function render()
    {
        return view('livewire.form-facturas', [
            'facturas' => Facturas::paginate(3),
            'empleados' => Empleados::where('name', 'LIKE', "%{$this->buscarVendedor}%")
            ->orWhere('cEmpleado', 'LIKE', "%{$this->buscarVendedor}%")
            ->paginate(3),
            
        ]);
    }
    public function loadSeller($id){
        $vendedor = Empleados::find($id);
        $this->idVendedor =  $vendedor->id;
        $this->nameTerceroVendedor =  $vendedor->name;
        $this->nameVendedor =  $vendedor->name;
        $this->cEmpleado =  $vendedor->cEmpleado;
        $this->reset(['buscarVendedor']);

    }
    public function changeView($data){        
        $this->view = $data;

    }
}
