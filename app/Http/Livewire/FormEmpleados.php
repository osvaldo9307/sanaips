<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Empleados;
use Livewire\WithPagination;


class FormEmpleados extends Component
{
    public $viewButton = 'store';
    public $cEmpleado, $name, $cargo, $buscar, $empleadoId;
    protected $listeners = [
        'confirmed',
        'cancelled',        
    ];
    use WithPagination;
    public function render()
    {
        return view('livewire.form-empleados' , [
            'empleados' => Empleados::where('name', 'LIKE', "%{$this->buscar}%")
            ->orWhere('cargo', 'LIKE', "%{$this->buscar}%")
            ->orWhere('cEmpleado', 'LIKE', "%{$this->buscar}%")
            ->orderBy('id','DESC')
            ->paginate(6)
        ]);
    }
    public function store(){
        $messages = [
            'name.required' => 'El campo nombre es obligatorio ',
            'cargo.required' => 'El campo cargo es obligatorio',
            'cEmpleado.required' => 'El campo código es obligatorio',
            'cEmpleado.unique' => 'El código del  empleado ya está en uso.',
        ];
        $this->validate([
            'name' => 'required',
            'cEmpleado' => 'required|unique:empleados',
            'cargo' => 'required',
        ], $messages);

        Empleados::create([
            'name' => $this->name,
            'cargo' => $this->cargo,
            'cEmpleado' => $this->cEmpleado,

        ]);
        $this->reset(['name', 'cEmpleado', 'cargo']);

        $this->alert('success', 'Empleado creado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function edit($id)
    {
            $empleado = Empleados::find($id);
            $this->empleadoId = $empleado->id;
            $this->name = $empleado->name;
            $this->cargo = $empleado->cargo;
            $this->cEmpleado = $empleado->cEmpleado;
            $this->viewButton = "edit";
    }
    public function update()
    {
        $messages = [
            'name.required' => 'El campo nombre es obligatorio ',
            'cargo.required' => 'El campo cargo es obligatorio',
            'cEmpleado.required' => 'El campo código es obligatorio',
        ];
        $this->validate([
            'name' => 'required',
            'cEmpleado' => 'required',
            'cargo' => 'required',
        ], $messages);

        $empleado = Empleados::find($this->empleadoId);
        $empleado->update([            
            'name' => $this->name,
            'cargo' => $this->cargo,
            'cEmpleado' => $this->cEmpleado,
        ]);
        $this->reset(['name', 'cargo', 'cEmpleado', 'viewButton']);        
        $this->alert('success', 'Empleado actualizado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function confirmed()
    {
        // Example code inside confirmed callback
        Empleados::destroy($this->empleadoId);
        $this->alert(
            'success',
            'Usuario eliminado correctamente'
        );
    }

    public function cancelled()
    {
        // Example code inside cancelled callback

        $this->alert('info', 'Cancelado');
    }
    public function destroy($id)
    {   
        $this->empleadoId = $id;
        $this->confirm('¿Estas seguro?', [
            'toast' => false,
            'position' => 'center',
            'showConfirmButton' => true,
            'confirmButtonText' =>  'Eliminar', 
            'cancelButtonText' => 'Cancelar',
            'onConfirmed' => 'confirmed',
            'onCancelled' => 'cancelled'
        ]);
    }

    public function cancel()
    {
        $this->reset(['name', 'cargo', 'cEmpleado', 'viewButton']);

    }
}
