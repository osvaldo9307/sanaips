<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div>
        <div>
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <!-- component -->
                
                <x-jet-welcome />
                {{-- <livewire:register-role /> --}}

            </div>
        </div>
    </div>
</x-app-layout>
