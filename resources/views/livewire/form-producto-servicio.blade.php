<div>
    <div class="grid grid-cols-1">
        <div class="grid grid-cols-2">
            <button wire:click="changeView('store')"
                class="inline-flex justify-center py-2 px-4 border border-transparent font-medium   text-black-300 focus:outline-none focus:bg-white ">Registrar
                tercero</button>

            <button wire:click="changeView('show')"
                class="inline-flex justify-center py-2 px-4 border border-transparent font-medium   text-black-300 focus:outline-none focus:bg-white">Listar
                terceros</button>
        </div>
        <div class=" bg-white grid grid-cols-1">
            @if ($view == 'show')
                <div class=" divide-x divide-gray-200 pb-5">
                    <div class="overflow-x-auto">
                        <div class="align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                {{-- {{ $buscar }} --}}
                                <div class="grid grid-cols-1 p-1">
                                    <input placeholder="Buscar..." wire:model="buscar" type="text" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>
                                @if ($terceros->count())
                                    <table class=" divide-y divide-gray-200">
                                        <thead class="bg-gray-50">
                                            <tr>
                                                <th scope="col"
                                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Identificación
                                                </th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Nombre
                                                </th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Razón social
                                                </th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Télefono
                                                </th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Ciudad
                                                </th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Tipo de tercero
                                                </th>

                                                <th scope="col" class="relative px-6 py-3">
                                                    <span class="sr-only">Edit</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-200">

                                            @foreach ($terceros as $tercero)
                                                <tr>
                                                    {{--<td
                                                        class="px-6 py-4 whitespace-nowrap">
                                                        <div class="flex items-center">

                                                            <div class="ml-4">
                                                                <div class="text-sm font-medium text-gray-900">
                                                                    {{ $tercero->identification }}
                                                                </div>
                                                                <div class="text-sm text-gray-500">
                                                                    {{ $tercero->identification }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td> --}}

                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">

                                                        {{ $tercero->typeIdentification }}
                                                        {{ $tercero->identification }}
                                                        @if ($tercero->dv)
                                                            -{{ $tercero->dv }}
                                                        @endif
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                        @if ($tercero->firstName || $tercero->secondName || $tercero->firtsLastName || $tercero->secondLastName)
                                                            {{ $tercero->firstName }} {{ $tercero->secondName }}
                                                            {{ $tercero->firtsLastName }} {{ $tercero->secondLastName }}
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                        @if ($tercero->razonSocial)
                                                            {{ $tercero->razonSocial }}
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                        {{ $tercero->phone }}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                        {{ $tercero->city }}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                        {{ $tercero->typeTercero }}
                                                    </td>
                                                    <td
                                                        class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                        <a wire:click="edit({{ $tercero->id }})" href="#"
                                                            class="text-indigo-600 hover:text-indigo-900"><i
                                                                class="fas fa-edit"></i></a>
                                                        <a wire:click="destroy({{ $tercero->id }})" href="#"
                                                            class="text-red-600 hover:text-red-900"><i
                                                                class="fas fa-trash"></i></a>
                                                    </td>

                                                </tr>
                                            @endforeach


                                            <!-- More rows... -->
                                        </tbody>
                                    </table>
                                    {{ $terceros->links() }}
                                @else
                                    <div class="px-4 py-5 bg-white sm:p-6 text-gray-500">

                                        No hay resultados para la búsqueda {{ $buscar }}

                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            @elseif($view == 'store')
                <div class="mt-5">
                    @if ($viewButton == 'store')
                        <form class="px-5" wire:submit.prevent="store">
                        @elseif($viewButton == 'edit')
                            <form class="px-5" wire:submit.prevent="update">
                    @endif
                    <div style="border: 1px solid #E5E7EB; " class="rounded p-3 mb-2">
                        <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-1">

                            <div class="mb-3">
                                <label for="identificador" class="block text-sm font-medium text-gray-700">Identificador
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="identifier" type="number"
                                    name="identificador" id="identificador" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('identifier')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3 col-span-2">
                                <label for="name" class="block text-sm font-medium text-gray-700">Nombre
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="name" type="text" name="name"
                                    id="name" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('name')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class=" mb-1">
                                <label for="barraReferencia"
                                    class="block text-sm font-medium text-gray-700">Barra/Referencia
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="barraReferencia" type="number"
                                    name="barraReferencia" id="second_name" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('barraReferencia')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class=" mb-1">
                                <label for="barraReferencia" class="block text-sm font-medium text-gray-700">

                                    Barra/Referencia
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="barraReferencia" type="text"
                                    name="barraReferencia" id="second_name" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('barraReferencia')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div wire:ignore.self class="mb-1">
                                <label for="type" class="block text-sm font-medium text-gray-700">Tipo
                                </label>
                                <select wire:model.lazy="type" id="type" name="type"
                                    class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    <option hidden>Seleccionar...</option>
                                    <option value="Servicio">Servicio</option>
                                    <option value="Producto">Producto</option>
                                    <option value="Activo">Activo</option>
                                    <option value="Especial">Especial</option>
                                </select>
                                @error('type')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-2 rounded p-3" style="border: 1px solid #E5E7EB;">
                        <div class="grid gap-1 grid-cols-2 rounded p-3" style="border: 1px solid #E5E7EB;">
                            <div class="mb-3">
                                <label for="iva" class="block text-sm font-medium text-gray-700">IVA
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="iva" type="number" name="iva"
                                    id="iva" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('iva')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="ico" class="block text-sm font-medium text-gray-700">ICO
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="ico" type="number" name="ico"
                                    id="ico" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('ico')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="costo" class="block text-sm font-medium text-gray-700">Costo
                                </label>
                                <input wire:keydown.debounce.50ms="calcularCosto"  style="text-transform:capitalize" wire:model="costo" type="number"
                                    name="costo" id="costo" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('costo')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="costoCalculado" class="block text-sm font-medium text-gray-700">Costo total
                                </label>
                                <input  style="text-transform:capitalize" wire:model.lazy="costoCalculado" type="text"
                                    name="costoCalculado" id="costoCalculado" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('costoCalculado')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="stock" class="block text-sm font-medium text-gray-700">Stock
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="stock" type="number"
                                    name="stock" id="stock" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('stock')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="registroInvima" class="block text-sm font-medium text-gray-700">Registro
                                    Invima
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="registroInvima" type="text"
                                    name="registroInvima" id="registroInvima" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('registroInvima')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="talla" class="block text-sm font-medium text-gray-700">Talla
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="talla" type="number"
                                    name="talla" id="talla" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('talla')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="pesoUnidad" class="block text-sm font-medium text-gray-700">Peso unidad
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="pesoUnidad" type="number"
                                    name="pesoUnidad" id="pesoUnidad" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('pesoUnidad')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                        <div class="rounded p-3" style="border: 1px solid #E5E7EB;">
                            <div class="grid gap-1 grid-cols-2 rounded p-3">
                                <div class="mb-3 col-span-2">
                                    <label for="marca" class="block text-sm font-medium text-gray-700">Marca
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="marca" type="text"
                                        name="marca" id="marca" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('marca')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="precioPublico" class="block text-sm font-medium text-gray-700">Precio
                                        Publico
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="precioPublico"
                                        type="number" name="precioPublico" id="precioPublico" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('precioPublico')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="valorUno" class="block text-sm font-medium text-gray-700">Valor venta dos
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="valorUno" type="number"
                                        name="valorUno" id="valorUno" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('valorUno')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="valorDos" class="block text-sm font-medium text-gray-700">Valor venta tres
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="valorDos" type="number"
                                        name="valorDos" id="valorDos" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('valorDos')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="valorTres" class="block text-sm font-medium text-gray-700">Valor venta cuatro
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="valorTres" type="number"
                                        name="valorTres" id="valorTres" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('valorTres')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="valorcuatro" class="block text-sm font-medium text-gray-700">Valor venta
                                        cinco
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="valorcuatro" type="number"
                                        name="valorcuatro" id="valorcuatro" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('valorcuatro')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mt-3">
                                    @if ($viewButton == 'store')

                                        <div class="px-4 py-3 text-right sm:px-6">

                                            <button type="submit"
                                                class="justify-right py-2 px-5 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                                <span wire:loading.attr="hidden">Guardar Registro</span>
                                                <span wire:loading wire:target="store">Procesando...</span>
                                            </button>

                                        </div>
                                    @elseif($viewButton == 'edit')
                                        <div class=" grid gap-1 grid-cols-2 px-4 py-3 text-right sm:px-6">
                                            <a role="button" wire:click="cancel"
                                                class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                                <span>Cancelar</span>
                                            </a>

                                            <button type="submit"
                                                class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                                <span wire:loading.attr="hidden">Actualizar</span>
                                                <span wire:loading wire:target="update">Procesando...</span>
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>


            @endif
        </div>
    </div>
</div>
