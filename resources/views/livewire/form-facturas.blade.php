<div>
    <div class="w-full bg-white grid grid-cols-1">
        <div class="grid grid-cols-2 bg-gray-100   text-center">
            <button wire:click="changeView('store')"
                class="inline-flex justify-center py-2 px-4 border border-transparent font-medium   text-black-300 focus:outline-none focus:bg-white ">Registrar
                factura</button>

            <button wire:click="changeView('show')"
                class="inline-flex justify-center py-2 px-4 border border-transparent font-medium   text-black-300 focus:outline-none focus:bg-white">Listar
                facturas</button>
        </div>
        @if ($view == 'show')
            <div class=" divide-x divide-gray-200 pb-5">
                <div class="overflow-x-auto">
                    <div class="align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            {{-- {{ $buscar }} --}}
                            <div class="grid grid-cols-1 p-1">
                                <input placeholder="Buscar..." wire:model="buscar" type="text" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>
                            @if ($facturas->count())
                                <table class=" divide-y divide-gray-200">
                                    <thead class="bg-gray-50">
                                        <tr>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Identificación
                                            </th>


                                            <th scope="col" class="relative px-6 py-3">
                                                <span class="sr-only">Edit</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">

                                        @foreach ($facturas as $tercero)
                                            <tr>

                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {{ $tercero->typeTercero }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                    <a wire:click="edit({{ $tercero->id }})" href="#"
                                                        class="text-indigo-600 hover:text-indigo-900"><i
                                                            class="fas fa-edit"></i></a>
                                                    <a wire:click="destroy({{ $tercero->id }})" href="#"
                                                        class="text-red-600 hover:text-red-900"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>

                                            </tr>
                                        @endforeach


                                        <!-- More rows... -->
                                    </tbody>
                                </table>
                                {{ $facturas->links() }}
                            @else
                                <div class="px-4 py-5 bg-white sm:p-6 text-gray-500">

                                    No hay resultados para la búsqueda {{ $buscar }}

                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        @elseif($view == 'store')
            <div class="mt-4">
                @if ($viewButton == 'store')
                    <form class="px-5" wire:submit.prevent="store">
                    @elseif($viewButton == 'edit')
                        <form class="px-5" wire:submit.prevent="update">
                @endif
                <div class="grid divide-gray-200 grid-cols-5 gap-2">
                    <div style="border: 1px solid #E5E7EB; " class="rounded p-3 col-span-2 ">
                        <div class=" mb-5">
                            <label for="fecha_ingreso" class="block text-sm font-medium text-gray-700">Fecha del ingreso
                            </label>
                            <input style="text-transform:capitalize" wire:model.lazy="fechaIngreso" type="date"
                                name="fecha_ingreso" id="fecha_ingreso" autocomplete="off"
                                class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('fechaIngreso')
                                <span class="text-sm text-red-600">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class=mb-5">
                            <div class="grid grid-cols-3 gap-1">
                                <div class="col-span-1">
                                    <label for="prefijo" class="block text-sm font-medium text-gray-700">Prefijo

                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="prefijo" type="text"
                                        name="prefijo" id="prefijo" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('prefijo')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-span-2" wire:ignore.self>
                                    <label for="n_factura" class="block text-sm font-medium text-gray-700">Número
                                        factura

                                    </label>
                                    <input disabled style="text-transform:capitalize" wire:model.lazy="nFactura"
                                        type="text" name="n_factura" id="n_factura" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('nFactura')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm font-medium text-gray-700">Producto /
                                Servicio
                            </label>
                            <div x-data="{ open: false }" class=" mt-1 w-full relative inline-block text-left">
                                <div>
                                    <a @click="open = true" type="button"
                                        class="inline-flex  w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
                                        id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        Seleccionar...
                                        <!-- Heroicon name: chevron-down -->
                                        <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd"
                                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </a>
                                </div>


                                <div x-show="open" @click.away="open = false"
                                    class=" w-full origin-top-center absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                                    <div class="py-1" role="menu" aria-orientation="vertical"
                                        aria-labelledby="options-menu">

                                        <a href="#"
                                            class="block px-2 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                            role="menuitem"><input class=" p-2 border-2 rounded h-8 w-full"></a>
                                        <a href="#"
                                            class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                            role="menuitem">Support</a>
                                        <a href="#"
                                            class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                            role="menuitem">License</a>
                                        <form method="POST" action="#">
                                            <button type="submit"
                                                class="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                                                role="menuitem">
                                                Sign out
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div style="border: 1px solid #E5E7EB; " class="rounded  p-3 col-span-3 ">
                        <p class="mb-4 text-base text-gray-700">Nombre: <span>{{ $nameTercero }}</span></p>
                        <p class="mb-4 text-base text-gray-700">Dirección: <span>{{ $adress }}</span></p>
                        <p class="mb-4 text-base text-gray-700">Ciudad: <span>{{ $city }}</span></p>
                        <p class="mb-4 text-base text-gray-700">Teléfono: <span>{{ $phone }}</span></p>
                        <p class="mb-4 text-base text-gray-700">Rte legal: <span>{{ $rLegal }}</span></p>
                        <p style="text-transform:capitalize"  class="text-base text-gray-700">Vendedor: <span> {{ $nameTerceroVendedor }}</span></p>
                    </div>

                </div>
                <div style="border: 1px solid #E5E7EB; " class=" m-5 rounded  p-3 col-span-3 ">
                    <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-1">


                        <div class=" mb-5">
                            <div class="grid grid-cols-6 gap-1">
                                <div class="col-span-2">
                                    <label for="centroCostos" class="block text-sm font-medium text-gray-700">Centro
                                        costos
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="centroCostos"
                                        type="number" name="centroCostos" id="centroCostos" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('centroCostos')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-span-4">
                                    <input disabled style="text-transform:capitalize" wire:model="centroCostosName"
                                        type="text" name="centroCostosName" id="centroCostosName" autocomplete="off"
                                        class="mt-6 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">

                                </div>

                            </div>

                        </div>
                        <div class=" mb-5">
                            <div class="grid grid-cols-6 gap-1">
                                <div class="col-span-2">
                                    <label for="plazo" class="block text-sm font-medium text-gray-700">Plazo
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="plazo" type="number"
                                        name="plazo" id="plazo" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('plazo')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-span-4">
                                    <input disabled style="text-transform:capitalize" wire:model="plazoName" type="text"
                                        name="plazoName" id="plazoName" autocomplete="off"
                                        class="mt-6 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">

                                </div>

                            </div>

                        </div>

                        <div class=" mb-5">
                            <div class="grid grid-cols-6 gap-1">
                                <div class="col-span-2">
                                    {{-- <label for="nameTerceroVendedor"
                                        class="block text-sm font-medium text-gray-700">Vendedor
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="nameTerceroVendedor"
                                        type="text" name="nameTerceroVendedor" id="nameTerceroVendedor"
                                        autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('nameTerceroVendedor')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror --}}
                                    <div>
                                        <label class="block text-sm font-medium text-gray-700">Vendedor
                                        </label>
                                        <div x-data="{ open: false }" class=" mt-1 w-full relative inline-block text-left">
                                            <div>
                                                @if ($cEmpleado)
                                                <a @click="open = true" type="button"
                                                class="inline-flex  w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
                                                id="options-menu" aria-haspopup="true" aria-expanded="true">
                                                {{$cEmpleado}}                                           
                                                <!-- Heroicon name: chevron-down -->
                                                <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                    <path fill-rule="evenodd"
                                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                        clip-rule="evenodd" />
                                                </svg>
                                            </a>
                                                @else
                                                <a @click="open = true" type="button"
                                                class="inline-flex  w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
                                                id="options-menu" aria-haspopup="true" aria-expanded="true">                                               
                                                Seleccionar...
                                                <!-- Heroicon name: chevron-down -->
                                                <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                    <path fill-rule="evenodd"
                                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                        clip-rule="evenodd" />
                                                </svg>
                                            </a>                                             
                                            @endif                                          
                                            </div>            
                                            <div x-show="open" @click.away="open = false"
                                                class="w-full origin-top-center absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                                                <div class="py-1" role="menu" aria-orientation="vertical"
                                                    aria-labelledby="options-menu">            
                                                    <a href="#"
                                                        class="block px-2 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                                        role="menuitem"><input wire:model="buscarVendedor" class=" p-2 border-2 rounded h-8 w-full"></a>   
                                                    @if ($buscarVendedor)
                                                    @foreach ($empleados as $empleado)                                                                     
                                                    <a @click="open = false" href="#"
                                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                                        role="menuitem"
                                                        wire:click="loadSeller({{$empleado->id}})"
                                                        >{{$empleado->cEmpleado}} {{$empleado->name}}</a>                                                   
                                                    @endforeach   
                                                    @endif                                                                                                 
                                                </div>
                                            </div>
                                        </div>
            
                                    </div>
                                </div>
                                <div class="col-span-4">
                                    <input disabled style="text-transform:capitalize" wire:model="nameVendedor"
                                        type="text" name="nameVendedor" id="nameVendedor" autocomplete="off"
                                        class="mt-6 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">

                                </div>

                            </div>

                        </div>
                        <div class=" mb-5">
                            <label for="observaciones" class="block text-sm font-medium text-gray-700">Observaciones
                            </label>
                            <input style="text-transform:capitalize" wire:model.lazy="observaciones" type="text"
                                name="observaciones" id="observaciones" autocomplete="off"
                                class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('observaciones')
                                <span class="text-sm text-red-600">{{ $message }}</span>
                            @enderror
                        </div>
                        {{-- <div class=" mb-5">
                            <label class="block text-sm font-medium text-gray-700">Producto /
                                Servicio
                            </label>
                            <div x-data="{ open: false }" class=" mt-1 w-full relative inline-block text-left">
                                <div>
                                    <a @click="open = true" type="button"
                                        class="inline-flex  w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
                                        id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        Seleccionar...
                                        <!-- Heroicon name: chevron-down -->
                                        <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd"
                                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </a>
                                </div>


                                <div x-show="open" @click.away="open = false"
                                    class=" w-full origin-top-center absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                                    <div class="py-1" role="menu" aria-orientation="vertical"
                                        aria-labelledby="options-menu">

                                        <a href="#"
                                            class="block px-2 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                            role="menuitem"><input class=" p-2 border-2 rounded h-8 w-full"></a>
                                        <a href="#"
                                            class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                            role="menuitem">Support</a>
                                        <a href="#"
                                            class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                                            role="menuitem">License</a>
                                        <form method="POST" action="#">
                                            <button type="submit"
                                                class="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                                                role="menuitem">
                                                Sign out
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="mb-5">
                            <div class="grid grid-cols-3 gap-1">
                                <div class="col-span-1">
                                    <label for="cantidad" class="block text-sm font-medium text-gray-700">Cantidad
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="productoServicio"
                                        type="number" name="cantidad" id="cantidad" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('productoServicio')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="col-span-1">
                                    <label for="iva" class="block text-sm font-medium text-gray-700">IVA
                                    </label>
                                    <input wire:keydown="doSomething" style="text-transform:capitalize"
                                        wire:model.lazy="productoServicio" type="number" name="iva" id="iva"
                                        autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('productoServicio')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-span-1">
                                    <label for="descuento" class="block text-sm font-medium text-gray-700">Descuento
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="descuento" type="number"
                                        name="descuento" id="descuento" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('descuento')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="mb-5">
                            <label for="total" class="block text-sm font-medium text-gray-700">Total
                            </label>
                            <input disabled style="text-transform:capitalize" wire:model.lazy="total" type="number"
                                name="total" id="total" autocomplete="off"
                                class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('total')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                            @enderror

                        </div>
                        <div class="mb-5 ">
                            <label class="block text-sm font-medium text-gray-700">
                            </label>
                            <!-- This example requires Tailwind CSS v2.0+ -->
                        </div> --}}

                    </div>
                </div>



                @if ($viewButton == 'store')

                    <div class="px-4 py-3 text-right sm:px-6">

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">{{ __('Save') }} </span>
                            <span wire:loading wire:target="store">Procesando...</span>
                        </button>

                    </div>
                @elseif($viewButton == 'edit')
                    <div class="px-4 py-3 text-right sm:px-6">
                        <a role="button" wire:click="cancel"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span>Cancelar</span>
                        </a>

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">Actualizar</span>
                            <span wire:loading wire:target="update">Procesando...</span>
                        </button>
                    </div>
                @endif
            </div>
        @endif
    </div>


</div>
