<div>
    <div class="w-full bg-white grid grid-cols-1">
        <div class="grid grid-cols-2 bg-gray-100   text-center">               
                <button wire:click="changeView('store')"
                
                    class="inline-flex justify-center py-2 px-4 border border-transparent font-medium   text-black-300 focus:outline-none focus:bg-white ">Registrar
                    tercero</button>
            
                <button wire:click="changeView('show')"
                    class="inline-flex justify-center py-2 px-4 border border-transparent font-medium   text-black-300 focus:outline-none focus:bg-white">Listar
                    terceros</button>
        </div>
        @if ($view == 'show')
            <div class=" divide-x divide-gray-200 pb-5">
                <div class="overflow-x-auto">
                    <div class="align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            {{-- {{ $buscar }} --}}
                            <div class="grid grid-cols-1 p-1">
                                <input placeholder="Buscar..." wire:model="buscar"
                                    type="text"  autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>
                            @if ($terceros->count())
                                <table class=" divide-y divide-gray-200">
                                    <thead class="bg-gray-50">
                                        <tr>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Identificación
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Nombre
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Razón social
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Télefono
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Ciudad
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Tipo de tercero
                                            </th>

                                            <th scope="col" class="relative px-6 py-3">
                                                <span class="sr-only">Edit</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">

                                        @foreach ($terceros as $tercero)
                                            <tr>
                                                {{--<td
                                                    class="px-6 py-4 whitespace-nowrap">
                                                    <div class="flex items-center">

                                                        <div class="ml-4">
                                                            <div class="text-sm font-medium text-gray-900">
                                                                {{ $tercero->identification }}
                                                            </div>
                                                            <div class="text-sm text-gray-500">
                                                                {{ $tercero->identification }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td> --}}

                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">

                                                    {{ $tercero->typeIdentification }} {{ $tercero->identification }}
                                                    @if ($tercero->dv)
                                                        -{{ $tercero->dv }}
                                                    @endif
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    @if ($tercero->firstName || $tercero->secondName || $tercero->firtsLastName || $tercero->secondLastName)
                                                        {{ $tercero->firstName }} {{ $tercero->secondName }}
                                                        {{ $tercero->firtsLastName }} {{ $tercero->secondLastName }}
                                                    @else
                                                        N/A
                                                    @endif

                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    @if ($tercero->razonSocial)
                                                        {{ $tercero->razonSocial }}
                                                    @else
                                                        N/A
                                                    @endif

                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">                                                    
                                                    {{ $tercero->phone}}                                                   
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">                                                    
                                                    {{ $tercero->city}}                                                   
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">                                                    
                                                    {{ $tercero->typeTercero }}                                                   
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                    <a wire:click="edit({{ $tercero->id }})" href="#"
                                                        class="text-indigo-600 hover:text-indigo-900"><i
                                                            class="fas fa-edit"></i></a>
                                                    <a wire:click="destroy({{ $tercero->id }})" href="#"
                                                        class="text-red-600 hover:text-red-900"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>

                                            </tr>
                                        @endforeach


                                        <!-- More rows... -->
                                    </tbody>
                                </table>
                                {{ $terceros->links() }}                            
                            @else
                                <div class="px-4 py-5 bg-white sm:p-6 text-gray-500">

                                    No hay resultados para la búsqueda {{ $buscar }}
                                    
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        @elseif($view == 'store')
            <div class="mt-5">
                @if ($viewButton == 'store')                
                    <form class="px-5" wire:submit.prevent="store">
                @elseif($viewButton == 'edit')
                    <form class="px-5" wire:submit.prevent="update">
                @endif
                <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-1">
                    <div class="mb-5">
                        <label for="country" class="block text-sm font-medium text-gray-700">Tipo
                            identificación</label>
                        <select wire:model.lazy="typeIdentification" id="country" name="country"
                            autocomplete="country"
                            class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option hidden>Seleccionar...</option>
                            <option value="TI">TI</option>
                            <option value="CC">CC</option>
                            <option value="CE">CE</option>
                            <option value="NIT">NIT</option>
                        </select>
                        @error('typeIdentification')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class= mb-5">
                        <div class="grid grid-cols-3 gap-1">
                            <div class="col-span-2">
                                <label for="identification"
                                    class="block text-sm font-medium text-gray-700">Identificación
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="identification"
                                    type="number" name="identification" id="identification" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('identification')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-span-1">
                                <label for="dv" class="block text-sm font-medium text-gray-700">D.V
                                </label>
                                <input style="text-transform:capitalize" wire:model.lazy="dv" type="number"
                                    name="dv" id="dv" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @error('dv')
                                    <span class="text-sm text-red-600">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class=" mb-5">
                        <label for="first_name" class="block text-sm font-medium text-gray-700">Primer Nombre
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="firstName" type="text"
                            name="first_name" id="first_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('firstName')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class=" mb-5">
                        <label for="second_name" class="block text-sm font-medium text-gray-700">Segundo Nombre
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="secondName" type="text"
                            name="second_name" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('name')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class=" mb-5">
                        <label for="firtsLastName" class="block text-sm font-medium text-gray-700">Primer apellido
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="firtsLastName" type="text"
                            name="firtsLastName" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('firtsLastName')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class=" mb-5">
                        <label for="secondLastName" class="block text-sm font-medium text-gray-700">Segundo apellido
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="secondLastName" type="text"
                            name="secondLastName" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('secondLastName')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class=" mb-5">
                        <label for="razonSocial" class="block text-sm font-medium text-gray-700">Razón social
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="razonSocial" type="text"
                            name="razonSocial" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('razonSocial')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class=" mb-5">
                        <label for="address" class="block text-sm font-medium text-gray-700">Dirección
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="address" type="text"
                            name="address" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('address')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div wire:ignore.self class="mb-5">
                        <label for="city" class="block text-sm font-medium text-gray-700">Ciudad</label>
                        <select wire:model.lazy="city" id="city" name="city" autocomplete="country"
                            class="select2 mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option hidden>Seleccionar...</option>
                            @foreach ($locations as $location)
                                <option value="{{ $location }}">{{ $location }}</option>
                            @endforeach
                        </select>
                        @error('city')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div wire:ignore.self class="mb-5">
                        <label for="typeTercero" class="block text-sm font-medium text-gray-700">Tipo
                            tercero</label>
                        <select wire:model.lazy="typeTercero" id="typeTercero" name="typeTercero"
                            autocomplete="country"
                            class="selecttype mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option hidden>Seleccionar...</option>
                            <option value="Cliente">Cliente</option>
                            <option value="Provedores">Provedores</option>
                            <option value="Médico">Médico</option>
                            <option value="Entidad">Entidad</option>
                            <option value="Enfermero">Enfermero</option>
                            <option value="Odontólogo">Odontólogo</option>
                        </select>
                        @error('typeTercero')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-5">
                        <label for="phone" class="block text-sm font-medium text-gray-700">Teléfono
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="phone" type="number" name="phone"
                            id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('phone')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-5">
                        <label for="rLegal" class="block text-sm font-medium text-gray-700">Representación legal
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="rLegal" type="text" name="rLegal"
                            id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('rLegal')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-5 col-span-3">
                        <label for="acompañante" class="block text-sm font-medium text-gray-700">Acompañante
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="acompañante" type="text"
                            name="acompañante" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('acompañante')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    

                </div>

                <div class="grid grid-cols-3 min-w-full">
                    <div class="mb-5">
                        <label class="inline-flex items-center">
                            <input wire:model.lazy="rFuente" type="checkbox" class="form-radio rounded" name="rFuente">
                            <span class="ml-2 text-sm text-gray-500">
                                Retención fuente</span>
                        </label>
                    </div>
                    
                    <label class="inline-flex items-center ml-6">
                        <input wire:model.lazy="rIVA" type="checkbox" class="form-radio rounded" name="rIVA">
                        <span class="ml-2 text-sm text-gray-500">
                            Retención IVA</span>
                    </label>
                    <label class="inline-flex items-center ml-6">
                        <input wire:model.lazy="rIndustraComercio" type="checkbox" class="form-radio rounded"
                            name="rIndustraComercio">
                        <span class="form-checkbox ml-2 text-sm text-gray-500">
                            Retención industria y comercio</span>
                    </label>

                </div>
                @if ($viewButton == 'store')

                    <div class="px-4 py-3 text-right sm:px-6">

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">{{ __('Save') }} </span>
                            <span wire:loading wire:target="store">Procesando...</span>
                        </button>

                    </div>
                @elseif($viewButton == 'edit')
                    <div class="px-4 py-3 text-right sm:px-6">
                        <a role="button" wire:click="cancel"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span>Cancelar</span>
                        </a>

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">Actualizar</span>
                            <span wire:loading wire:target="update">Procesando...</span>
                        </button>
                    </div>
                @endif
            </div>
        @endif
    </div>


</div>

<script>
    document.addEventListener('livewire:load', function() {
        $(document).ready(function() {
            $('.select2').select2();
            $('.selecttype').select2();
            $('.select2').on('change', function(e) {
                @this.set('city', e.target.value);
            });
            $('.selecttype').on('change', function(e) {
                @this.set('typeTercero', e.target.value);
            });
        });
    })

</script>
