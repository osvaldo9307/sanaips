<div>
    <div>
        <div>
            <div class="min-w-full bg-white grid sm:grid-cols-1 md:grid-cols-2 overflow-hidden  ">
                <!-- component -->
                <div class="min-w-full divide-x divide-gray-200">
                    <div class=" overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                {{-- {{$buscar}} --}}
                                <div class="grid grid-cols-1 p-5">
                                    <input placeholder="Buscar..."  wire:model="buscar" type="text"
                                        name="first_name" id="first_name" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>
                                @if ($users->count())
                                <table class="w-full divide-y divide-gray-200">
                                    <thead class="bg-gray-50">
                                        <tr>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Usuario
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                identificación
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Role
                                            </th>

                                            <th scope="col" class="relative px-6 py-3">
                                                <span class="sr-only">Edit</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">

                                        @foreach ($users as $user)


                                            <tr>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="flex items-center">
                                                        <div class="flex-shrink-0 h-10 w-10">
                                                            <img class="h-10 w-10 rounded-full"
                                                                src="{{ $user->profile_photo_url }}"
                                                                alt="{{ $user->name }}">
                                                        </div>
                                                        <div class="ml-4">
                                                            <div class="text-sm font-medium text-gray-900">
                                                                {{ $user->name }}
                                                            </div>
                                                            <div class="text-sm text-gray-500">
                                                                {{ $user->email }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="flex items-center">
                                                        
                                                        <div class="ml-4">
                                                            <div class="text-sm font-medium text-gray-900">
                                                                {{ $user->typeIdentification }}
                                                            </div>
                                                            <div class="text-sm text-gray-500">
                                                                {{ $user->identification }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {{ $user->role }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                    <a wire:click="edit({{ $user->id }})" href="#"
                                                        class="text-indigo-600 hover:text-indigo-900"><i
                                                            class="fas fa-edit"></i></a>
                                                    <a wire:click="destroy({{ $user->id }})" href="#"
                                                        class="text-red-600 hover:text-red-900"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach


                                        <!-- More rows... -->
                                    </tbody>
                                </table>
                                {{ $users->links() }}
                                @else 
                                <div class="px-4 py-5 bg-white sm:p-6 text-gray-500">
                                    No hay resultados para la búsqueda {{$buscar}}
                                </div>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    
                    @if ($view == 'store')
                        
                        <form class="px-5  " wire:submit.prevent="store">
                        @elseif($view == 'edit')

                        <form class="px-5 " wire:submit.prevent="update">
                    @endif
                                    
                    <div class="overflow-hidden">
                        <div class="px-4 bg-white sm:p-6">
                            <div class="grid grid-cols-12">
                                <div class="col-span-12 sm:col-span-12 mb-5">
                                    <label for="first_name" class="block text-sm font-medium text-gray-700">Nombres
                                        y apellidos</label>
                                    <input style="text-transform:capitalize" wire:model.lazy="name" type="text"
                                        name="first_name" id="first_name" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('name')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="col-span-12 sm:col-span-12 mb-5">
                                    <label for="country" class="block text-sm font-medium text-gray-700">Tipo identificación                                        </label>
                                    <select wire:model.lazy="typeIdentification" id="country" name="country" autocomplete="country"
                                        class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                        <option hidden>Seleccionar...</option>
                                        <option value="TI">TI</option>
                                        <option value="CC">CC</option>
                                        <option value="CE">CE</option>
                                    </select>
                                    @error('typeIdentification')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-span-12 sm:col-span-12 mb-5">
                                    <label for="first_name" class="block text-sm font-medium text-gray-700">Numero identificación
                                    </label>
                                    <input style="text-transform:capitalize" wire:model.lazy="identification" type="number"
                                        name="first_name" id="first_name" autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('identification')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="col-span-12 sm:col-span-12 mb-5">
                                    <label for="email_address" class="block text-sm font-medium text-gray-700">Correo
                                    </label>
                                    <input wire:model.lazy="email" type="email" name="email_address" id="email_address"
                                        autocomplete="off"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('email')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>
                                @if ($view == 'store')
                                    <div class="col-span-12 sm:col-span-12 mb-5">
                                        <label for="street_address"
                                            class="block text-sm font-medium text-gray-700">Contraseña</label>
                                        <input wire:model.lazy="password" type="password" name="street_address"
                                            id="street_address" autocomplete="off"
                                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                        @error('password')
                                            <span class="text-sm text-red-600">{{ $message }}</span>
                                        @enderror
                                    </div>
                                @endif

                                <div class="col-span-12 sm:col-span-12 mb-5">
                                    <label for="country" class="block text-sm font-medium text-gray-700">Role /
                                        Cargo</label>
                                    <select wire:model.lazy="role" id="country" name="country" autocomplete="country"
                                        class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                        <option hidden>Seleccionar...</option>
                                        <option value="admin">Administrador</option>
                                        <option value="medico">Médico</option>
                                        <option value="paciente">Paciente</option>
                                    </select>
                                    @error('role')
                                        <span class="text-sm text-red-600">{{ $message }}</span>
                                    @enderror
                                </div>


                            </div>
                        </div>


                        @if ($view == 'store')
                        
                            <div class="px-4 py-3 text-right sm:px-6">
                                <button type="submit"
                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    <span wire:loading.attr="hidden">{{ __('Save') }} </span>
                                    <span wire:loading wire:target="store">Procesando...</span>
                                </button>
                            </div>
                        @elseif($view == 'edit')
                            <div class="px-4 py-3 text-right sm:px-6">
                                <a  role="button" wire:click="cancel"
                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    <span>Cancelar</span>
                                </a>
                        
                                <button type="submit"
                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    <span wire:loading.attr="hidden">Actualizar</span>
                                    <span wire:loading wire:target="update">Procesando...</span>
                                </button>
                            </div>
                        @endif

                    </div>
                    </form>
                    
                </div>

            </div>

        </div>
    </div>
</div>
</div>
