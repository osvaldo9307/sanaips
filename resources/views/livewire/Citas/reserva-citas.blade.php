<div class="container"><br><br>

      
    @if(session('estado'))
 
    
      <div class="alert alert-success" id="exitoso" role="alert">
        <i class="fas fa-smile-beam"></i> <strong>Excelente !!</strong> Tu cita ha sido reservada exitosamente!
      </div>
    @endif

    

   


    <div class="card">
        <h5 class="card-header">Reserva Tu Cita</h5>
        <div class="card-body">
          <form method="POST"  action="{{ route('Lista_CitasPos') }}  " > 
                <div class="row justify-content-right">
                   <!-- TOCKED DE CADA FORMULARIO -->
               @csrf
        
                   <div class="col-md-6" style="width: 50%;">
                    <label>Paciente</label>
                         @error('Paciente')
                         <span class="text-sm text-red-600">{{ $message }}</span>
                         @enderror
                    <select name="Paciente" class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" value="{{ old('Paciente') }}">
                        <option name="">Seleccione</option>
                        @forelse ($medicos as $listpacientes) 
                          @if($listpacientes->typeTercero=='Cliente') 
                            <option value="{{ $listpacientes->id }}">{{ $listpacientes->firstName }}</option>
                          @endif
                        @empty
                         NO HAY DATOS
                        @endforelse 
                    </select>
                    </div>
           
                   <div class="col-md-6" style="width: 50%;">
                   <label>Medico</label>
                        @error('medico')
                        <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                   <select name="medico" class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                       <option name="">Seleccione</option>

                       @forelse ($medicos as $listmedicos) 
                        @if($listmedicos->typeTercero=='Médico') 
                            <option value="{{ $listmedicos->id }}">{{ $listmedicos->firstName }}</option>
                        @endif
                       @empty
			            NO HAY DATOS
		               @endforelse
                   </select>
                   </div>

                  
                   <div class="col-md-6" style="width: 50%;">
                       <label>Fecha Inicio </label>
                            @error('fecha')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                            @enderror
                       <input type="text"  id="fecha" name="fecha" class="datetimepicker mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"  value="{{ old('fecha') }}"><br>
                   </div>


                

                    <div class="col-md-10"></div>
                    <div class="col-md-2" align="right"><button class="btn btn-primary btn-lg btn-block" >Enviar</button></div>

                    
                </div>
               </form>
        </div>
      </div>
</div>