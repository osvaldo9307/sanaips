<div>
    <div class="grid bg-white  sm:grid-cols-1 lg:grid-cols-2">
        {{-- Lista empleados --}}
        <div>
            {{-- @foreach ($empleados as $empleado)
                <p>{{$empleado->cEmpleado}}{{$empleado->name}}{{$empleado->cargo}}</p>               
            @endforeach --}}
            <div class="min-w-full divide-x divide-gray-200">
                <div class=" overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            {{-- {{$buscar}} --}}
                            <div class="grid grid-cols-1 p-5">
                                <input placeholder="Buscar..."  wire:model="buscar" type="text"
                                    name="first_name" id="first_name" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>
                            @if ($empleados->count())
                            <table class="w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Código
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Nombre
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Cargo
                                        </th>

                                        <th scope="col" class="relative px-6 py-3">
                                            <span class="sr-only">Edit</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">

                                    @foreach ($empleados as $empleado)


                                        <tr>
                                            
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="flex items-center">
                                                    
                                                    <div class="ml-4">
                                                        <div class="text-sm font-medium text-gray-900">
                                                            {{ $empleado->cEmpleado }}
                                                        </div>
                                
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="flex items-center">
                                                    
                                                    <div class="ml-4">
                                                        <div class="text-sm font-medium text-gray-900">
                                                            {{ $empleado->name }}
                                                        </div>
                                
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                {{ $empleado->cargo }}
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                <a wire:click="edit({{ $empleado->id }})" href="#"
                                                    class="text-indigo-600 hover:text-indigo-900"><i
                                                        class="fas fa-edit"></i></a>
                                                <a wire:click="destroy({{ $empleado->id }})" href="#"
                                                    class="text-red-600 hover:text-red-900"><i
                                                        class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach


                                    <!-- More rows... -->
                                </tbody>
                            </table>
                            <div class="m-5">
                                {{ $empleados->links() }}
                            </div>                           
                            @else 
                            <div class="px-4 py-5 bg-white sm:p-6 text-gray-500">
                                No hay resultados para la búsqueda {{$buscar}}
                            </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="mt-5">
                @if ($viewButton == 'store')
                    <form class="px-5" wire:submit.prevent="store">
                    @elseif($viewButton == 'edit')
                        <form class="px-5" wire:submit.prevent="update">
                @endif
                <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-1">
                    <div class="mb-5 col-span-3">
                        <label for="cEmpleado" class="block text-sm font-medium text-gray-700">Código del empleado
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="cEmpleado" type="number"
                            name="cEmpleado" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('cEmpleado')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-6 col-span-3">
                        <label for="name" class="block text-sm font-medium text-gray-700">Nombre del empleado
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="name" type="text" name="name"
                            id="name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('name')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-4 col-span-3">
                        <label for="cargo" class="block text-sm font-medium text-gray-700">Cargo del empleado</label>
                        <select wire:model.lazy="cargo" id="cargo" name="cargo" autocomplete="cargo"
                            class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option hidden>Seleccionar...</option>
                            <option value="Vendedor">Vendedor</option>
                            <option value="Mecánico">Mecánico</option>
                            <option value="Tecnico">Tecnico</option>
                            <option value="Técnico">Técnico</option>
                        </select>
                        @error('cargo')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                @if ($viewButton == 'store')

                    <div class="px-4 py-3 text-right sm:px-6">

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">{{ __('Save') }} </span>
                            <span wire:loading wire:target="store">Procesando...</span>
                        </button>

                    </div>
                @elseif($viewButton == 'edit')
                    <div class="px-4 py-3 text-right sm:px-6">
                        <a role="button" wire:click="cancel"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span>Cancelar</span>
                        </a>

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">Actualizar</span>
                            <span wire:loading wire:target="update">Procesando...</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
